import storage from 'node-persist'  
import {order} from 'types'

export default class Models{
    static getOrder(){
        return storage.getItem('orders')
    }

    static setOrder(orders: order){
        return storage.setItem('orders', orders)
    }
}