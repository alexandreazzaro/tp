import App from './app';
import express from 'express'
import http from "http";

export default class Serveur {
  public port: number | boolean;
  public app: express.Application;

  constructor(port: number | boolean) {
    this.port = port;
    this.app = new App().getInstance();
  }

  public start(): void {
    //const port = this.normalizePort(process.env.PORT || 12345);
    this.app.set("port", this.port);

    const server = http.createServer(this.app);
    server.listen(this.port);
    server.on("error",(error) => this.onError(error));
    //server.on("listening", this.onListening);
    server.on('listening', () => this.onListening())
  }

  normalizePort(value: any): number | boolean {
    const port = parseInt(value, 10);

    if (isNaN(port)) {
      return value;
    }

    if (port >= 0) {
      return port;
    }

    return false;
  }

  onError(error: any): never {
    if (error.syscall !== "listen") {
      throw error;
    }

    switch (error.code) {
      case "EACCES":
        console.error(`${this.port} requires elevated privileges`);
        process.exit(1);
      case "EADDRINUSE":
        console.error(`${this.port} is already in use`);
        process.exit(1);
      default:
        throw error;
    }
  }

  onListening(): void {
    console.log(`Listening on ${this.port}`);
  }
}