import { Router, Request, Response } from 'express'
import Models from './Models'
import {order} from 'types'
import adapter from './adapter'

export default class Controller{

    static hello(req: Request, res: Response){
        res.send('Hello World !')
    }

    static favi(req: Request, res: Response){
        res.status(204);
    }

    static async getOrder(req: Request, res: Response){
        const orders = await Models.getOrder();
        /*orders.array.forEach((element: order) => {
            new adapter(element);    
        });*/

        for(let i=0; i<orders.length; i++){
            new adapter(orders[i]).anonymOrder()
        }
        
        res.json(orders)
    }

    static async getOrderById(req: Request, res: Response){
        const id = req.params.id

        const orders = await Models.getOrder();
        const result = orders.find((order: order) => order.id === parseInt(id, 10))

        if (!result) {
            return res.sendStatus(404)
        }

        return res.json(result)
            }

    static async sendOrder(req: Request, res: Response){
        const payload = req.body

        const orders = await Models.getOrder();
        const alreadyExists = orders.find((order: order) => order.id === payload.id)

        if (alreadyExists) {
            return res.sendStatus(409)
        }

        orders.push(payload)

        await Models.setOrder(orders)

        res.sendStatus(201)
    }
}