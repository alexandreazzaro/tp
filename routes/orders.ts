import { Router, Request, Response } from 'express'
import Controller from '../Controller'

const router = Router()

router.get('/', async (req: Request, res: Response) => {
  Controller.getOrder(req, res);
})

router.get('/:id', async (req: Request, res: Response) => {
  Controller.getOrderById(req, res);
})

router.post('/', async (req: Request, res: Response) => {
  Controller.sendOrder(req, res);
})

export default router