import { Router, Request, Response } from 'express'
import Controller from '../Controller'

const router = Router()

router.get('/', async (req: Request, res: Response) => {
  Controller.hello( req, res);
})

router.get('/favicon.ico', (req: Request, res: Response) => Controller.favi(req, res));

export default router