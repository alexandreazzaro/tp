import Controller from "./Controller";
import { order, packages, delivery } from "types";

export default class adapter implements order {
  id: number;
  packages: packages;
  delivery: delivery;

  constructor(command: order) {
    this.id = command.id;
    this.packages = command.packages;
    this.delivery = command.delivery;
  }

  async anonymOrder() {
    const y = {
      fullname: "xxx",
      email: "xxx",
      phone: "xxx",
      address: "xxx",
      postalCode: "xxx",
      city: "xxx",
    };
    this.delivery.contact = y;
    /*this.delivery.contact.address = "XXX";
    this.delivery.contact.city = "XXX";
    this.delivery.contact.email = "XXX";
    this.delivery.contact.fullname = "XXX";
    this.delivery.contact.phone = "XXX";
    this.delivery.contact.postalCode = "XXX"; */
  }
}
