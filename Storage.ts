import storage from "node-persist";
import { order } from "types";
import { orders } from "./data/_orders";

export default class Storage {
  storeType: string;

  constructor(storeType: string) {
    this.storeType = storeType;
  }

  init() {
    // Init default data
    switch (this.storeType) {
      case "orders":
        storage.init().then(() => {
          storage.setItem(this.storeType, orders);
        });
        break;
    }
  }
}
