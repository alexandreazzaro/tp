interface order {
    id: number;
    packages: packages;
    delivery: delivery;
}

interface size {
  unit: string;
  value: number;
}

interface packages {
length: size;
width: size;
height: size;
weight: size;
products: products;
}

interface price {
  currency: string;
  value: number;
}

interface products {
  name: string;
  price: price;
}

interface delivery {
    storePickingInterval: intervale;
    deliveryInterval: intervale;
    contact: contact;
    carrier: carrier;
}

interface intervale {
    start: string;
    end: string;
  }

interface contact {
  fullname: string;
  email: string;
  phone: string;
  address: string;
  postalCode: string;
  city: string;
}

interface carrier {
    name: string;
}

export {order,packages,delivery}